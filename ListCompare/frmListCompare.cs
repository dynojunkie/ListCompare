﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListCompare
{
    public partial class frmListCompare : Form
    {
        public frmListCompare()
        {
            InitializeComponent();
            radioButton1.Checked = true;
            cbCaseSensitive.Checked = Properties.Settings.Default.caseSensitive;
            tbTrimCharacters.Text = Properties.Settings.Default.trimCharacters;
        }

        private void btnCompare_Click(object sender, EventArgs e)
        {
            compare();
        }

        private void compare()
        {
            richTextBoxResult.Clear();
            //radioButton1 = only items in list1
            //radioButton2 = only items in list2
            //radioButton3 = only items in both lists

            char[] trimCharacters = tbTrimCharacters.Text.ToCharArray();
            string[] list1 = StripChars(richTextBoxList1.Lines, trimCharacters);
            string[] list2 = StripChars(richTextBoxList2.Lines, trimCharacters);

            IEnumerable<string> differenceQuery;
            IEnumerable<string> both;
            string selectedRadioButton = "";
            StringComparer comparer;

            //Get selected compare option
            foreach (Control control in this.groupBox1.Controls)
            {
                if (control is RadioButton)
                {
                    RadioButton radio = control as RadioButton;
                    if (radio.Checked)
                    {
                        selectedRadioButton = radio.Name;
                    }
                }
            }

            //Shorthand if then else.
            comparer = (cbCaseSensitive.Checked) ? StringComparer.Ordinal : StringComparer.OrdinalIgnoreCase;

            switch (selectedRadioButton)
            {
                case "radioButton1":
                    differenceQuery = list1.Except(list2, comparer);
                    break;
                case "radioButton2":
                    differenceQuery = list2.Except(list1, comparer);
                    break;
                case "radioButton3":
                    differenceQuery = both = list1.Intersect(list2, comparer);
                    break;
                default:
                    differenceQuery = list1.Except(list2, comparer);
                    break;
            }

            // Execute the query.
            foreach (string s in differenceQuery)
                richTextBoxResult.Text += s + Environment.NewLine;
        }

        private static string[] StripChars(string[] lines, char[] chars)
        {
            List<string> lineList = new List<string>();
            try
            {
                foreach (string line in lines)
                {
                    lineList.Add(line.TrimStart(chars).TrimEnd(chars));
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            return lineList.ToArray();
        }   

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WakeOnHan.frmAbout about = new WakeOnHan.frmAbout();
            about.Show();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            richTextBoxResult.Clear();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            richTextBoxResult.Clear();
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            richTextBoxResult.Clear();
        }

        private void cbCaseSensitive_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.caseSensitive = cbCaseSensitive.Checked;
            Properties.Settings.Default.Save();
        }

        private void tbTrimCharacters_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.trimCharacters = tbTrimCharacters.Text;
            Properties.Settings.Default.Save();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Ensure that text is selected in the text box.   
            if (TextFocusedFirst().SelectionLength > 0)
                // Copy the selected text to the Clipboard.
                TextFocusedFirst().Copy();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Determine if there is any text in the Clipboard to paste into the text box.
            if (Clipboard.GetDataObject().GetDataPresent(DataFormats.Text) == true)
            {
                try
                {
                    // Paste current text in Clipboard into text box.
                    TextFocusedFirst().Paste();
                }
                catch(Exception err)
                {
                    MessageBox.Show(err.Message);
                }
            }
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TextFocusedFirst().SelectAll();
        }

        //Get the richtextbox that has focus
        private RichTextBox TextFocusedFirst()
        {
            // mainBox2 is a TextBox control.
            if (richTextBoxList1.Focused == true)
            {
                return richTextBoxList1;
            }
            // titleBox2 is a TextBox control.
            else if (richTextBoxList2.Focused == true)
            {
                return richTextBoxList2;
            }
            // passwordTextBox is a TextBox control.
            else if (richTextBoxResult.Focused == true)
            {
                return richTextBoxResult;
            }
            else
            {
                // If Nothing is focused, set focus to richTextBoxList1.
                richTextBoxList1.Focus();
                return richTextBoxList1; 
            }
        }

        private void richTextBoxList1_MouseDown(object sender, MouseEventArgs e)
        {
            ((Control)sender).Focus();
        }

        private void richTextBoxList2_MouseDown(object sender, MouseEventArgs e)
        {
            ((Control)sender).Focus();
        }

        private void richTextBoxResult_MouseDown(object sender, MouseEventArgs e)
        {
            ((Control)sender).Focus();
        }
    }
}
